<?php

//[1] Repetition Control Structures
//Repetition control structure are used to execute code multiple times

//While Loop
//While loop - takes a sinle condition
//if the condition evaluate to true, the code inside the block will run

function whileLoop() {

	$count = 5;

	while($count !== 0) {

		echo $count . '<br/>';
		$count--;
	}
}

//Do-While Loop
//do-while loop - it works  lot like the while loop, but unlike while loops, do while loop guarantee that the code will be execute at least once

function doWhileLoop() {

	 $count = 20;

	 do {
	 	echo $count. '<br/>';
	 	$count--;

	 }while ($count > 0);
}


// For Loop
/*
	for loop is more flexible than while and do-while loops consists of 3 parts:
	1; initial vlaue
	2. condition
	3. iteration
*/

function forLoop() {

	for($count = 0; $count <=20; $count++) {

		echo $count. '<br/>';
	}
}

// Preferred usage of while vs For Loops
// For loops are best used when the number of iteration is known beforehand (iterating over arrays)
//While Loops - are better when number of iterations is unknown at the loop is defined (depends on user input)

//Continue and Breaks statements
/*
*/

function modifiedForLoop() {

	for ($count = 0; $count <=20; $count++){
		if($count % 2 === 0){
			continue;
		}
		echo $count, '<br/>';
		if($count > 10){
			break;
		}
	}
}


//[2] Array Manipulation
//An array is a kind of variable that can hold more than one value 
//Array are declared using the square brackets '[]' or array() function

$studentNumbers = array('2020-1923','2020-1924','2020-1925','2020-1926','2020-1927');//Before
$studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; //Introduced


//Simple Arrays
$grades = [98.5,94.3,89.2,90.1];
$computerBrand = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu', 'Dell', 'HP'];
$task = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake react'
];

//Associative Array
$gradePeriods = ['firstGrading' => 98.5, 'secondGrading'=> 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

//Two-Dimensional Array
$heroes = [
	['ironman', 'thor', 'hulk'],
	['wolverine', 'cyclops', 'jean grey'],
	['batman', 'superman', 'wonderwoman']

];

//2-dimensional associative array
$ironManPowers = [
	'regular' => ['repulsor blast', 'rocket punch'],
	'signature' => ['unibeam']
];

//Array Mutations - seek to modify the contents of an array while array iterations aim to evaluate each element in an array

//Array Sorting
$sortedBrands = $computerBrand;
$reverseSortedBrands = $computerBrand;

sort($sortedBrands); //arrange by alphabetically
rsort($reverseSortedBrands); //arrange by alphabetically


function searchBrand($brands, $brand){
	return (in_array($brand, $brands)) ? "$brand is in the array." : "$brand in not in the array";

}

$reverseGradePeriods = array_reverse($gradePeriods);
