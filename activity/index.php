<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity s02</title>
</head>
<body>

	
	<h2>Divisible of Five</h2>
	<?= arrayLoop(); ?>

	<h2>Array Manipulation</h2>
	<?php array_push($students, 'John Smith'); ?>
	<p><?= print_r($students); ?></p>
    <p><?php echo count($students); ?></p>

	<?php array_push($students, 'Jane Smith'); ?>
	<p><?= print_r($students); ?></p>
	<p><?php echo count($students); ?></p>

	<?php array_shift($students); ?>
	<p><?= print_r($students); ?></p>
	<p><?php echo count($students); ?></p>

	
</body>
</html>